import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/*Importamos para poder utilizar todos los servicios de HttpClient*/
import { HttpClientModule } from '@angular/common/http';

/*Routes*/
import { APP_ROUTING } from './app.routes';

/*Components*/
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NabvarComponent } from './components/shared/nabvar/nabvar.component';
import { ArtistComponent } from './components/artist/artist.component';

/*Pipes*/
import { SinfotoPipe } from './pipes/sinfoto.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';

/*Services*/
import { SpotifyService } from './services/spotify.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NabvarComponent,
    SinfotoPipe,
    DomseguroPipe
    ArtistComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SpotifyService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
