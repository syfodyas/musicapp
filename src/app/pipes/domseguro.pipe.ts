import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {

  constructor( private __domSanitizer: DomSanitizer ){};

  transform(value: string, url: string): any {
    return this.__domSanitizer.bypassSecurityTrustResourceUrl(url + value);
  }

}
