import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sinfoto'
})
export class SinfotoPipe implements PipeTransform {

  transform(images: any[]): any {

    let no_image = 'assets/img/noimage.png';

    return (images.length > 0) ? images[1].url : no_image;
  }

}
