import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  artists: any[] = [];
  tracks: any[] = [];
  urlSpotify: string = 'https://api.spotify.com/v1/';
  token: string = 'BQC3seAPCGOQbzIczQiejjG7R9R1H6nZqr-__W6J0qDRCiJIGrzu1Gj7luLL0DWNgAR0Qa9Y223KHNLhu68';

  constructor(public __http: HttpClient) {
    console.log('Spotify Service Ready');
  }

  getArtists(termino: string){
    let url = `${this.urlSpotify}search?query=${ termino }&type=artist&offset=0&limit=20`;
    /*Aquí podemos especificar todos los headers que enviaremos en la petición
    Para poder utilizar este method primero debemos importarlo.
    */
    let headers = this.getHeaders();
    /*Esto es un observable, con lo que para poder escuchar los cambios en la respuesta,
    debo de subscribirme con subscribe. Pero no es recomendable poner el subscribe aquí por esto
    lo vamos a situar en el componente donde se utilice y aquí hacemos un return de la respuesta.*/
    /*Con el operador map puedo transformar la data o cambiarla completamente*/
    return this.__http.get(url, {headers: headers}).map((response:any) => {
      this.artists = response.artists.items;
      return this.artists;
    });
  }

  getTopTracks(artist_id: string){
    let url = `${this.urlSpotify}artists/${artist_id}/top-tracks?country=US`;
    let headers = this.getHeaders();

    return this.__http.get(url, {headers: headers}).map((response:any) => {
      this.tracks = response.tracks;
      return this.tracks;
    });
  }

  getArtist(artist_id: string){
    let url = `${this.urlSpotify}artists/${ artist_id }`;
    let headers = this.getHeaders();

    return this.__http.get(url, {headers: headers});
  }

  private getHeaders():HttpHeaders {
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token
    });

    return headers;
  }

}
