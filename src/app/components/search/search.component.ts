import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {
  termino: string = '';

  /*Injection Service Concept*/
  constructor(public __spotify: SpotifyService) {}

  searchArtists(){
    if(this.termino.length == 0){
      return;
    }

    this.__spotify.getArtists(this.termino).subscribe(response => {
      console.log(response);
    });
  }
}
