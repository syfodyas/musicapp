import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent implements OnInit {
  artist:any = {};
  tracks:any[] = [];
  imageArtist:string = '';

  constructor( private __activatedRoute: ActivatedRoute, public __spotifyService: SpotifyService ) {

  }

  ngOnInit() {
    this.__activatedRoute.params.map( params => params.artist_id ).subscribe( artist_id => {
      this.__spotifyService.getArtist( artist_id ).subscribe( response => {
        this.artist = response;
        this.imageArtist = this.artist.images[2].url;
      } );

      this.__spotifyService.getTopTracks( artist_id ).subscribe( response => {
        this.tracks = response;
        console.log(this.tracks);
      } );
    } );
  }

}
